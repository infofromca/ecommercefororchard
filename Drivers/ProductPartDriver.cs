﻿using Orchard.ContentManagement;
using Orchard.ContentManagement.Drivers;
using Skywalker.Webshop.Models;
using Orchard.ContentManagement.Handlers;
using System;
using System.Globalization;

namespace Skywalker.Webshop.Drivers
{
    public class ProductPartDriver : ContentPartDriver<ProductPart> {

        protected override string Prefix {
            get { return "Product"; }
        }

       
        //protected override DriverResult Display(ProductPart part, string displayType, dynamic shapeHelper)
        //{
        //    var driverResults = new List<DriverResult> {
        //        ContentShape("Parts_Product", () => shapeHelper.Parts_Product(
        //            Price: part.Price,
        //            Sku: part.Sku
        //        ))
        //    };

        //    if (displayType != "SummaryAdmin")
        //    {
        //        driverResults.Add
        //        (
        //            ContentShape("Parts_Product_AddButton", () => 
        //                shapeHelper.Parts_Product_AddButton(ProductId: part.Id))
        //        );
        //    }

        //    return Combined(driverResults.ToArray());
        //}

        protected override DriverResult Display(ProductPart part, string displayType, dynamic shapeHelper)
        {
            return Combined(
                ContentShape("Parts_Product_SummaryAdmin", () => shapeHelper.Parts_Product_SummaryAdmin(
                    Price: part.UnitPrice,
                    Sku: part.Sku
                )),
                ContentShape("Parts_Product", () => shapeHelper.Parts_Product(
                    Price: part.UnitPrice,
                    Sku: part.Sku
                )),
                 ContentShape("Parts_Product_AddButton", () => shapeHelper.Parts_Product_AddButton(
                     ProductId: part.Id
                     ))
                );
        }

        protected override DriverResult Editor(ProductPart part, dynamic shapeHelper) {
            return ContentShape("Parts_Product_Edit", () => shapeHelper
                .EditorTemplate(TemplateName: "Parts/Product", Model: part, Prefix: Prefix));
        }

        protected override DriverResult Editor(ProductPart part, IUpdateModel updater, dynamic shapeHelper) {
            updater.TryUpdateModel(part, Prefix, null, null);
            return Editor(part, shapeHelper);
        }

        
        protected override void Importing(ProductPart part, ImportContentContext context)
        {
            var sku = context.Attribute(part.PartDefinition.Name, "Sku");
            if (!String.IsNullOrWhiteSpace(sku))
            {
                part.Sku = sku;
            }
            var priceString = context.Attribute(part.PartDefinition.Name, "UnitPrice");
            decimal price;
            if (decimal.TryParse(priceString, NumberStyles.Currency, CultureInfo.InvariantCulture, out price))
            {
                part.UnitPrice = price;
            }

            //var isDigitalAttribute = context.Attribute(part.PartDefinition.Name, "IsDigital");
            //bool isDigital;
            //if (bool.TryParse(isDigitalAttribute, out isDigital))
            //{
            //    part.IsDigital = isDigital;
            //}
            //var weightString = context.Attribute(part.PartDefinition.Name, "Weight");
            //double weight;
            //if (double.TryParse(weightString, NumberStyles.Float, CultureInfo.InvariantCulture, out weight))
            //{
            //    part.Weight = weight;
            //}
            //var shippingCostString = context.Attribute(part.PartDefinition.Name, "ShippingCost");
            //double shippingCost;
            //if (shippingCostString != null && double.TryParse(shippingCostString, NumberStyles.Currency, CultureInfo.InvariantCulture, out shippingCost))
            //{
            //    part.ShippingCost = shippingCost;
            //}
        }

        protected override void Exporting(ProductPart part, ExportContentContext context)
        {
            context.Element(part.PartDefinition.Name).SetAttributeValue("Sku", part.Sku);
            context.Element(part.PartDefinition.Name).SetAttributeValue("UnitPrice", part.UnitPrice.ToString("C"));
            //context.Element(part.PartDefinition.Name).SetAttributeValue("IsDigital", part.IsDigital.ToString(CultureInfo.InvariantCulture).ToLower());
            //context.Element(part.PartDefinition.Name).SetAttributeValue("Weight", part.Weight.ToString(CultureInfo.InvariantCulture));
            //if (part.ShippingCost != null)
            //{
            //    context.Element(part.PartDefinition.Name).SetAttributeValue(
            //        "ShippingCost", ((double)part.ShippingCost).ToString("C"));
            //}
        }

    }
}