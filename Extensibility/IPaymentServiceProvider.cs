using Orchard.Events;

namespace Skywalker.Webshop.Extensibility {
    public interface IPaymentServiceProvider : IEventHandler {
        void RequestPayment(PaymentRequest e);
        void ProcessResponse(PaymentResponse e);
    }
}