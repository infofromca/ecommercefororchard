﻿using System.Web.Mvc;
using Skywalker.Webshop.Models;

namespace Skywalker.Webshop.Extensibility
{
    public class PaymentRequest
    {
        public OrderRecord Order { get; private set; }
        public bool WillHandlePayment { get; set; }
        public ActionResult ActionResult { get; set; }

        public PaymentRequest(OrderRecord order) {
            Order = order;
        }
    }
}