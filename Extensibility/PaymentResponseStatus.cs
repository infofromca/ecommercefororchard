namespace Skywalker.Webshop.Extensibility
{
    public enum PaymentResponseStatus {
        Success,
        Failed,
        Cancelled,
        Exception
    }
}