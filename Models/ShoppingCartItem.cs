using System;

namespace Skywalker.Webshop.Models
{
    [Serializable]
    public sealed class ShoppingCartItem {
        public int ProductId { get; private set; }

        private int _quantity;
        public int Quantity {
            get { return _quantity; }
            set {  //by zg
                if (value < 0)
                {
                    _quantity = 0;// throw new IndexOutOfRangeException();
                }
                else
                {
                    _quantity = value;
                }
                
            }
        }

        public ShoppingCartItem() {
        }

        public ShoppingCartItem(int productId, int quantity = 1) {
            ProductId = productId;
            Quantity = quantity;
        }
    }

}