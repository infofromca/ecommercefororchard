using Orchard;
using Skywalker.Webshop.Models;
using Orchard.ContentManagement;
using System.Collections.Generic;

namespace Skywalker.Webshop.Services
{
    public interface ICustomerService : IDependency {
        CustomerPart CreateCustomer(string email, string password);
        AddressPart GetAddress(int customerId, string addressType);
        AddressPart CreateAddress(int customerId, string addressType);

        IContentQuery<CustomerPart> GetCustomers();
        CustomerPart GetCustomer(int id);
        AddressPart GetAddress(int id);
        IEnumerable<AddressPart> GetAddresses(int customerId);
    }
}