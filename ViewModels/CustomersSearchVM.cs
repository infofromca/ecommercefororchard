namespace Skywalker.Webshop.ViewModels {
    public class CustomersSearchVM {
        public string Expression { get; set; }
    }
}